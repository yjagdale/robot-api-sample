*** Settings ***
Resource  ${EXECDIR}/variables.robot
Resource  ${EXECDIR}/src/services/reqres/list-users/list-users.robot

*** Test Cases ***
Get all users
    Log                             Base Url Is ${BASE_URL}
    ${users}=                       Get Users          ${BASE_URL}
    Should Be Equal As Strings      1                  ${users.json()}[page]