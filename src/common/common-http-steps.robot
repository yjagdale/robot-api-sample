*** Settings ***
Library               RequestsLibrary
Library               Collections
*** Keywords ***
Start Session With Server
    [Arguments]     ${BASE_URL}
    Create Session  rest-session  ${BASE_URL}

Get From Service
    [Arguments]             ${ENDPOINT}         ${HEADERS}
    ${response}=            GET On Session      rest-session        ${ENDPOINT}
    Return From Keyword     ${response}