*** Settings ***
Resource  ${EXECDIR}/src/common/common-http-steps.robot

*** Variables ***
${GET_USER_ENDPOINT}=      /api/users

*** Keywords ***
Get Users
    [Arguments]                     ${BASE_URL}
    Start Session With Server       ${BASE_URL}
    ${response}=                    Get From Service                ${GET_USER_ENDPOINT}          TEST
    Return From Keyword             ${response}